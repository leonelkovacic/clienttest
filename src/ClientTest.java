import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ClientTest implements Runnable{

	private int amount;
		
	public static void main(String[] args) throws InterruptedException {
		
		ExecutorService executor = Executors.newFixedThreadPool(1);
		for (int i = 1; i < 3; i++) {			
			executor.submit(new ClientTest(2 * i));
		}
		executor.shutdown();
		executor.awaitTermination(1, TimeUnit.MINUTES);
		
	}
	
	public ClientTest(int amount) {
		super();
		this.amount = amount;
	}

	public void run() {
		try {
			Socket socket = new Socket("localhost", 8080);
			OutputStream os = socket.getOutputStream();
			InputStream is = socket.getInputStream();
			os.write(String.valueOf(this.amount).getBytes());
			byte [] buff = new byte [1024 * 1024 * 10];
			for (int i = 0; i < this.amount; i++) {				
				int length = is.read(buff);
				String msg = new String(buff,0,length,Charset.defaultCharset());
				String espectedMsg = "Mensaje " + i + "\n";
				if (!msg.equals(espectedMsg)) {
					socket.close();
					throw new Exception("Se esperaba " + espectedMsg + " y se recibió " + msg);
				} else {
					System.out.println( msg + " OK!");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
